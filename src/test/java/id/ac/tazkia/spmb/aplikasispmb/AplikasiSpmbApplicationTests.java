package id.ac.tazkia.spmb.aplikasispmb;

import id.ac.tazkia.spmb.aplikasispmb.service.ApiService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AplikasiSpmbApplicationTests {

	@Autowired
	private ApiService apiService;

	@Test
	public void contextLoads() {
		apiService.insertMahasiswa("2110101001");
	}

}
