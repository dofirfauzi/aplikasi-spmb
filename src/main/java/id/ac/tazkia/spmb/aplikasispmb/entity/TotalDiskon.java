package id.ac.tazkia.spmb.aplikasispmb.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Entity @Data
@Table(
        name="total_diskon",
        uniqueConstraints =
        @UniqueConstraint(columnNames = {"bulan", "total"})
)
public class TotalDiskon {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "bulan", nullable = false)
    @NotNull
    private String bulan;

    @NotNull
    @Min(0)
    private BigDecimal total;
}