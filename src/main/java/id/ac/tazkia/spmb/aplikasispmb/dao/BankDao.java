package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Bank;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BankDao extends PagingAndSortingRepository<Bank,String> {
}
