package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.CicilanDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.HasilTestDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import id.ac.tazkia.spmb.aplikasispmb.service.TagihanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.parameters.P;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
public class TagihanController {
    private static final Logger logger = LoggerFactory.getLogger(TagihanController.class);

    @Autowired
    private UserDao userDao;
    @Autowired
    private LeadsDao leadsDao;
    @Autowired
    private PendaftarDao pendaftarDao;
    @Autowired
    private TagihanDao tagihanDao;
    @Autowired
    private VirtualAccountDao virtualAccountDao;
    @Autowired
    private PembayaranDao pembayaranDao;
    @Value("classpath:sample/panduanPembayaran.pdf")
    private Resource panduanPembayaran;
    @Autowired
    private JenisBiayaDao jenisBiayaDao;
    @ModelAttribute("listJenisBiaya")
    public Iterable<JenisBiaya> listJenisBiaya(){
        return jenisBiayaDao.findAll();
    }
    @Autowired
    public TagihanService tagihanService;
    @Autowired
    public  HasilTestDao hasilTestDao;
    @Autowired
    public  JadwalTestDao jadwalTestDao;
    @Autowired
    public  PeriodeDao  periodeDao;
    @Autowired
    public  ProgramStudiDao  programStudiDao;
    @Autowired
    public  GradeDao gradeDao;
    @Autowired
    public  NilaiBiayaDao nilaiBiayaDao;
    @Autowired
    public  TahunAjaranDao tahunAjaranDao;
    @Autowired
    public CicilanDao cicilanDao;
    @Autowired
    public DiskonDao diskonDao;


//    @GetMapping("/panduanPembayaran")
//    public void  getPanduanPembayaran(HttpServletResponse response) throws Exception {
//        response.setContentType("application/pdf");
//        response.setHeader("Content-Disposition", "attachment; filename=Panduan_Pembayaran.pdf");
//        FileCopyUtils.copy(panduanPembayaran.getInputStream(), response.getOutputStream());
//        response.getOutputStream().flush();
//    }


    @GetMapping("/tagihan/listPendaftar")
    public void listTagihanPendaftar(Authentication currentUser, Model model, Pageable page){
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }
        Leads leads = leadsDao.findByUser(u);
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        if (pendaftar != null) {
            model.addAttribute("pendaftar", pendaftar);
        }

        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());
            }

        Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pendaftar, jenisBiaya);
            if (tagihan == null){
                logger.warn("Tagihan dengan pendaftar {} tidak ada dalam database", pendaftar.getLeads().getNama());
            }
        model.addAttribute("tagihan", tagihan);

        Page<VirtualAccount> virtualAccount = virtualAccountDao.findByTagihan(tagihan, page);
        if (virtualAccount == null) {
            logger.warn("Virtual account dengan no tagihan {} tidak ada dalam database", tagihan.getNomorTagihan());
        }
        model.addAttribute("virtualAccount", virtualAccount);


        if (tagihan != null) {
            Pembayaran pembayaran = pembayaranDao.findByTagihan(tagihan);
            if (pembayaran == null) {
                logger.warn("Tagihan {} belum dilunasi", tagihan.getNomorTagihan());
            }
            model.addAttribute("pembayaran", pembayaran);
        }
    }


    @GetMapping("/tagihan/list")
    public void listTagihan(@RequestParam(required = false)String nama, @RequestParam(required = false)JenisBiaya jenisBiaya
                            ,Model m,Pageable page){
        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        if (nama != null && jenisBiaya != null) {
            m.addAttribute("nama", nama);
            m.addAttribute("jenisBiaya", jenisBiaya);
            m.addAttribute("daftarTagihan", tagihanDao.findByPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarTahunAjaranAndJenisBiaya(nama,ta,jenisBiaya, page));
        } else if (nama != null && jenisBiaya == null) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarTagihan", tagihanDao.findByPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarTahunAjaran(nama,ta,page));
        } else if (nama == null && jenisBiaya != null) {
            m.addAttribute("jenisBiaya", jenisBiaya);
            m.addAttribute("daftarTagihan", tagihanDao.findByJenisBiayaAndPendaftarTahunAjaran(jenisBiaya,ta, page));
        } else {
            m.addAttribute("daftarTagihan", tagihanDao.findByPendaftarTahunAjaran(ta,page));
        }
    }

    @GetMapping("/tagihan/detailTagihan")
    public void detailTagihan(@RequestParam(required = true) String idTagihan,@RequestParam(required = true) String jenisBiaya,Model model,Pageable pageable){
        model.addAttribute("jenisBiaya", jenisBiaya);

        if (jenisBiaya.equals(AppConstants.JENIS_BIAYA_PENDAFTARAN)) {
            Page<Tagihan> tagihan = tagihanDao.findById(idTagihan, pageable);
            model.addAttribute("tagihan", tagihan);

            Tagihan tagihan1 = new Tagihan();
            tagihan1.setId(idTagihan);

            model.addAttribute("virtualAccount", virtualAccountDao.findByTagihan(tagihan1, pageable));
            Pembayaran pembayaran = pembayaranDao.findByTagihan(tagihan1);
            model.addAttribute("pembayaran", pembayaran);

        }
        else if (jenisBiaya.equals(AppConstants.JENIS_BIAYA_DAFTAR_ULANG)) {
            Page<Tagihan> tagihan = tagihanDao.findByPendaftarIdAndJenisBiayaId(idTagihan, jenisBiaya,pageable);
            model.addAttribute("tagihan", tagihan);

            for (Tagihan tg : tagihan) {
                model.addAttribute("nama", tg.getPendaftar().getLeads().getNama());
                model.addAttribute("noReg", tg.getPendaftar().getNomorRegistrasi());
                model.addAttribute("prodi", tg.getPendaftar().getProgramStudi().getNama());
                model.addAttribute("konsen", tg.getPendaftar().getKonsentrasi());
                model.addAttribute("totalTagihan", tg.getTotalTagihan());

                BigDecimal sCicilan = cicilanDao.itungNominalCicilandanStatus(tg.getPendaftar(), Boolean.FALSE);
                BigDecimal tBelumLunas = tagihanDao.totalBayar(Boolean.FALSE,tg.getPendaftar());

                System.out.println("Sisa Cicilan : " + sCicilan);
                System.out.println("Belum Lunas : " + tBelumLunas);
                if (sCicilan != null && tBelumLunas != null) {
                    BigDecimal tSisa = new BigDecimal(sCicilan.intValue() + tBelumLunas.intValue());
                    model.addAttribute("sisaTagihan", tSisa);
                } else if (sCicilan != null && tBelumLunas == null) {
                    model.addAttribute("sisaTagihan", sCicilan);
                }else  if (sCicilan == null && tBelumLunas !=  null){
                    model.addAttribute("sisaTagihan", tBelumLunas);
                }

                BigDecimal tBayar = tagihanDao.totalBayar(Boolean.TRUE,tg.getPendaftar());
                model.addAttribute("totalBayar", tBayar);



//CEK HASIL TEST
                JadwalTest jt = jadwalTestDao.findByPendaftar(tg.getPendaftar());
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                model.addAttribute("keterangan", hasilTest.getKeterangan());

//Tagihan Belum Lunas
                if (tg.getLunas()) {
                    model.addAttribute("tgBlmLunas", tg.getNomorTagihan());
                }

                JenisBiaya jb = new JenisBiaya();
                jb.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);
                Tagihan tgblm = tagihanDao.findByPendaftarAndJenisBiayaAndLunas(tg.getPendaftar(), jb, false);
                model.addAttribute("virtualAccountBlm", virtualAccountDao.findByTagihan(tgblm, pageable));


                Pembayaran pembayaran = pembayaranDao.findByTagihan(tg);
                model.addAttribute("pembayaran", pembayaran);

                model.addAttribute("virtualAccount", virtualAccountDao.findByTagihan(tg, pageable));

                Page<VirtualAccount> virtualAccounts =  virtualAccountDao.findByTagihan(tg, pageable);
                for (VirtualAccount va : virtualAccounts) {
                    if (tg.getLunas() == true && pembayaran.getBank() == va.getBank()) {
                        model.addAttribute("vaBank", va.getBank());
                        model.addAttribute("vaNama", va.getBank().getNamaBank());
                        model.addAttribute("vaNo", va.getNomorVa());
                    } else {
                        logger.debug("Tagihan Belum Lunas");
                    }

                }

//CICILAN
                model.addAttribute("cicilan", cicilanDao.findByPendaftarAndStatusOrderByUrutanCicilanAsc(tg.getPendaftar(),Boolean.FALSE, pageable));

            }
        }
    }

    @PostMapping("/kirimNotifikasi/tagihan")
    public String kirimNotif (@RequestParam Pendaftar id){
        if (id == null) {
            logger.debug("Id {} tidak ada dalam database.", id);
        }
        tagihanService.prosesTagihanPendaftaran(id);
        logger.info("Tagihan dengan no debitur {} berhasil dikirim", id.getNomorRegistrasi());

        return "redirect:/pendaftar/list";

    }

    @RequestMapping(value = "/daftarUlang/form", method = RequestMethod.GET)
    public void tampilkanForm(@RequestParam(value = "id", required = true) String id,
                              @RequestParam(required = false) String error,Pageable page,
                              Model m){

        Pendaftar p = pendaftarDao.findById(id).get();
        JadwalTest jt = jadwalTestDao.findByPendaftar(p);
        m.addAttribute("pendaftar", p);

        HasilTest d = hasilTestDao.findByJadwalTest(jt);
        List<Periode> periode = periodeDao.cariPeriodeUntukTanggal(d.getJadwalTest().getTanggalTest());
        HasilTestDto hasilTestDto = new HasilTestDto();

        logger.debug("Jumlah data : {}", periode.size());

        for (Periode periode1 : periode) {
            hasilTestDto.setPeriode(periode1);
            hasilTestDto.setId(d.getId());
            hasilTestDto.setJadwalTest(jt);
            hasilTestDto.setGrade(d.getGrade());
            hasilTestDto.setJenisTest(d.getJadwalTest().getJenisTest());
            logger.debug(hasilTestDto.getId() + " " + hasilTestDto.getPeriode());

            JenisBiaya jenisBiaya = jenisBiayaDao.findById(AppConstants.JENIS_BIAYA_DAFTAR_ULANG).get();
            ProgramStudi programStudi = programStudiDao.findById(hasilTestDto.getJadwalTest().getPendaftar().getProgramStudi().getId()).get();
            Periode pr = periodeDao.findById(hasilTestDto.getPeriode().getId()).get();
            Grade gd = gradeDao.findById(hasilTestDto.getGrade().getId()).get();
            List<NilaiBiaya> nilaiBiaya = nilaiBiayaDao.findByProgramStudiAndJenisBiayaAndPeriodeAndGrade(programStudi,jenisBiaya,pr, gd);


            if (hasilTestDto.getJadwalTest().getPendaftar().getLeads().getJenjang() == Jenjang.S1) {
                for (NilaiBiaya nilaiBiaya1 : nilaiBiaya) {
                    Integer nilaiDiskon = nilaiBiaya1.getNilai().intValue() * 50 / 100;
                    NilaiBiaya nbDis = new NilaiBiaya();
                    nbDis.setNilai(new BigDecimal(nilaiDiskon));
                    hasilTestDto.setNilaiBiaya(nbDis);

                }
            }
            if (hasilTestDto.getJadwalTest().getPendaftar().getLeads().getJenjang() == Jenjang.S2) {
                for (NilaiBiaya nilaiBiaya1 : nilaiBiaya) {
                    if (hasilTestDto.getJadwalTest().getPendaftar().getProgramStudi().getId().equals("008") &&
                        hasilTestDto.getJadwalTest().getPendaftar().getKonsentrasi().equals("Reguler")) {
                        System.out.println("Konsentrasi Reguler : " + hasilTestDto.getJadwalTest().getPendaftar().getKonsentrasi() );
                        NilaiBiaya nbMes = new NilaiBiaya();
                        nbMes.setNilai(new BigDecimal(6000000));
                        hasilTestDto.setNilaiBiaya(nbMes);
                    } else if (hasilTestDto.getJadwalTest().getPendaftar().getKonsentrasi().equals("Eksekutif") &&
                            hasilTestDto.getJadwalTest().getPendaftar().getProgramStudi().getId().equals("008")) {
                        System.out.println("Konsentrasi Eksekutif : " + hasilTestDto.getJadwalTest().getPendaftar().getKonsentrasi() );
                        NilaiBiaya nbMes = new NilaiBiaya();
                        nbMes.setNilai(new BigDecimal(7000000));
                        hasilTestDto.setNilaiBiaya(nbMes);
                    } else {
                        System.out.println("Nilai Biayanya : " + nilaiBiaya1.getNilai());
                        hasilTestDto.setNilaiBiaya(nilaiBiaya1);
                    }
                }
            }

            if (d != null){
                m.addAttribute("hasil", hasilTestDto);
            }
        }
        logger.debug("Nomor Registrasi :"+ p.getNomorRegistrasi());

        JenisBiaya jenisBiaya = jenisBiayaDao.findById(AppConstants.JENIS_BIAYA_DAFTAR_ULANG).get();
        ProgramStudi programStudi = programStudiDao.findById(p.getProgramStudi().getId()).get();
        m.addAttribute("daftarNilai", nilaiBiayaDao.findByJenisBiayaAndProgramStudi(jenisBiaya,programStudi, page));;

        Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiayaAndLunas(p,jenisBiaya, false);
        if (tagihan == null){
            logger.debug("Tagihan dengan no registrasi : {} tidak ditemukan", p.getNomorRegistrasi());
        }
        m.addAttribute("tagihan", tagihan);

        m.addAttribute("virtualAccount", virtualAccountDao.findByTagihan(tagihan, page));

        if (tagihan != null) {
            Pembayaran cekpembayaran = pembayaranDao.findByTagihan(tagihan);
            if (cekpembayaran == null) {
                logger.warn("Tagihan {} belum dilunasi", tagihan.getNomorTagihan());
            }
            m.addAttribute("cekpembayaran", cekpembayaran);
        }

        Tagihan tLunas = tagihanDao.findByPendaftarAndJenisBiayaAndLunas(p,jenisBiaya, true);
        Page<Pembayaran> pembayaran = pembayaranDao.findByTagihan(tLunas, page);
        if (pembayaran == null) {
            logger.debug("Tagihan dengan no tagihan : {} belum dilunasi", tLunas.getNomorTagihan());
        }
        m.addAttribute("pembayaran", pembayaran);

    }

//    @RequestMapping(value = "/daftarUlang/form", method = RequestMethod.POST)
//    public String prosesForm(@Valid HasilTestDto hasilTestDto,
//                             BindingResult errors){
//
//        HasilTest hasilTest = hasilTestDao.findByJadwalTest(hasilTestDto.getJadwalTest());
//        hasilTest.setKeterangan(hasilTestDto.getKeterangan());
//        hasilTestDao.save(hasilTest);
//
//        tagihanService.createTagihanDaftarUlang(hasilTest.getJadwalTest().getPendaftar(),hasilTestDto.getNilai());
//
//        return "redirect:/pendaftar/list";
//    }

    @GetMapping("/daftarUlang/listPendaftar")
    public void listDaftarUlang(Authentication currentUser, Model model, Pageable page){
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }
        Leads leads = leadsDao.findByUser(u);
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        if (pendaftar != null) {
            model.addAttribute("pendaftar", pendaftar);
        }

        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);
        if (jenisBiaya == null) {
            logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());
        }

        Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pendaftar, jenisBiaya);
        if (tagihan == null){
            logger.warn("Tagihan dengan pendaftar {} tidak ada dalam database", pendaftar.getLeads().getNama());
        }
        model.addAttribute("tagihan", tagihan);

        Pendaftar p = pendaftarDao.findById(pendaftar.getId()).get();
        JadwalTest jt = jadwalTestDao.findByPendaftar(p);

        if (tagihan != null) {
            HasilTest d = hasilTestDao.findByJadwalTest(jt);
            List<Periode> periode = periodeDao.cariPeriodeUntukTanggal(d.getJadwalTest().getTanggalTest());
            HasilTestDto hasilTestDto = new HasilTestDto();
            logger.debug("Jumlah data : {}", periode.size());


            for (Periode periode1 : periode) {
                hasilTestDto.setPeriode(periode1);
                hasilTestDto.setId(d.getId());
                hasilTestDto.setJadwalTest(jt);
                hasilTestDto.setGrade(d.getGrade());
                hasilTestDto.setJenisTest(d.getJadwalTest().getJenisTest());
                hasilTestDto.setKeterangan(d.getKeterangan());
                logger.debug(hasilTestDto.getId() + " " + hasilTestDto.getPeriode());

                JenisBiaya jb = jenisBiayaDao.findById(AppConstants.JENIS_BIAYA_DAFTAR_ULANG).get();
                ProgramStudi programStudi = programStudiDao.findById(hasilTestDto.getJadwalTest().getPendaftar().getProgramStudi().getId()).get();
                Periode pr = periodeDao.findById(hasilTestDto.getPeriode().getId()).get();
                Grade gd = gradeDao.findById(hasilTestDto.getGrade().getId()).get();
                List<NilaiBiaya> nilaiBiaya = nilaiBiayaDao.findByProgramStudiAndJenisBiayaAndPeriodeAndGrade(programStudi,jb,pr, gd);
                for (NilaiBiaya nilaiBiaya1 : nilaiBiaya) {
                    hasilTestDto.setNilaiBiaya(nilaiBiaya1);
                }
                if (d != null){
                    model.addAttribute("hasil", hasilTestDto);
                }

//                int up = hasilTestDto.getNilaiBiaya().getNilai().intValue() - 1500000;;
                model.addAttribute("nilaiUp",  hasilTestDto.getNilaiBiaya().getNilai());

            }

//            int upb = tagihan.getNilai().intValue() - 1500000;
            model.addAttribute("nilaiUpB", tagihan.getNilai());
        }

        Page<VirtualAccount> virtualAccount = virtualAccountDao.findByTagihan(tagihan, page);
        if (virtualAccount == null) {
            logger.warn("Virtual account dengan no tagihan {} tidak ada dalam database", tagihan.getNomorTagihan());
        }
        model.addAttribute("virtualAccount", virtualAccount);


        if (tagihan != null) {
            Pembayaran pembayaran = pembayaranDao.findByTagihan(tagihan);
            if (pembayaran == null) {
                logger.warn("Tagihan {} belum dilunasi", tagihan.getNomorTagihan());
            }
            model.addAttribute("pembayaran", pembayaran);
        }

    }


    @GetMapping("/daftarUlang/formPendaftar")
    public void formDuPendaftar(Authentication currentUser, Model model, Pageable page){
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }
        Leads leads = leadsDao.findByUser(u);
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        if (pendaftar != null) {
            model.addAttribute("pendaftar", pendaftar);
        }

        JadwalTest jt = jadwalTestDao.findByPendaftar(pendaftar);
        //CEK HASIL TEST
        model.addAttribute("hasilTest", hasilTestDao.findByJadwalTest(jt));

        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);

        Page<Tagihan> tagihan = tagihanDao.findByPendaftarIdAndJenisBiayaId(pendaftar.getId(), jenisBiaya.getId(), page);
        model.addAttribute("tagihan", tagihan.getTotalPages());


        if (tagihan.getTotalPages() == 0) {
            logger.warn("Tagihan dengan pendaftar {} tidak ada dalam database", pendaftar.getLeads().getNama());

            //CEK HASIL TEST
            HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
            //CEK TAGIHAN DAFTAR ULANG
            if (hasilTest != null) {
                HasilTest d = hasilTestDao.findByJadwalTest(jt);
                List<Periode> periode = periodeDao.cariPeriodeUntukTanggal(d.getJadwalTest().getTanggalTest());
                HasilTestDto hasilTestDto = new HasilTestDto();
                logger.debug("Jumlah data : {}", periode.size());

                //Cek nilai tagihan
                for (Periode periode1 : periode) {
                    hasilTestDto.setPeriode(periode1);
                    hasilTestDto.setId(d.getId());
                    hasilTestDto.setJadwalTest(jt);
                    hasilTestDto.setGrade(d.getGrade());
                    hasilTestDto.setJenisTest(d.getJadwalTest().getJenisTest());
                    hasilTestDto.setKeterangan(d.getKeterangan());
                    logger.debug(hasilTestDto.getId() + " " + hasilTestDto.getPeriode());

                    JenisBiaya jb = jenisBiayaDao.findById(AppConstants.JENIS_BIAYA_DAFTAR_ULANG).get();
                    ProgramStudi programStudi = programStudiDao.findById(hasilTestDto.getJadwalTest().getPendaftar().getProgramStudi().getId()).get();
                    Periode pr = periodeDao.findById(hasilTestDto.getPeriode().getId()).get();
                    Grade gd = gradeDao.findById(hasilTestDto.getGrade().getId()).get();

                    List<NilaiBiaya> nilaiBiaya = nilaiBiayaDao.findByProgramStudiAndJenisBiayaAndPeriodeAndGrade(programStudi, jb, pr, gd);
                    for (NilaiBiaya nilaiBiaya1 : nilaiBiaya) {
                        hasilTestDto.setNilaiBiaya(nilaiBiaya1);
                    }
                    JenisBiaya jenisAsrama = new JenisBiaya();
                    jenisAsrama.setId(AppConstants.JENIS_BIAYA_ASRAMA);
                    List<NilaiBiaya> nilaiAsrama = nilaiBiayaDao.findByProgramStudiAndJenisBiayaAndPeriodeAndGrade(programStudi, jenisAsrama, pr, gd);
                    for (NilaiBiaya na : nilaiAsrama) {
                        model.addAttribute("asrama", na.getNilai());
                        hasilTestDto.setNilaiAsrama(na.getNilai());
                    }
                    JenisBiaya jenisUkt = new JenisBiaya();
                    jenisUkt.setId(AppConstants.JENIS_BIAYA_UKT);
                    List<NilaiBiaya> nilaiUkt = nilaiBiayaDao.findByProgramStudiAndJenisBiayaAndPeriodeAndGrade(programStudi, jenisUkt, pr, gd);
                    for (NilaiBiaya nu : nilaiUkt) {
                        model.addAttribute("ukt", nu.getNilai());
                        hasilTestDto.setNilaiUkt(nu.getNilai());
                    }
                    JenisBiaya jenisPen = new JenisBiaya();
                    jenisPen.setId(AppConstants.JENIS_BIAYA_WISUDA);
                    List<NilaiBiaya> nilaiPen = nilaiBiayaDao.findByProgramStudiAndJenisBiayaAndPeriodeAndGrade(programStudi, jenisPen, pr, gd);
                    for (NilaiBiaya np : nilaiPen) {
                        model.addAttribute("wisuda", np.getNilai());
                        hasilTestDto.setNilaiWisuda(np.getNilai());
                    }
                    if (d != null) {
                        model.addAttribute("hasil", hasilTestDto);
                    }
                }


                //SET DISKON PERBULAN
                List<Diskon> diskonList = diskonDao.cariDiskonUntukTanggal(LocalDate.now());
                if (hasilTest.getJadwalTest().getPendaftar().getLeads().getJenjang().equals(Jenjang.S1)) {
                    if (diskonList != null) {
                        for (Diskon cekDiskon : diskonList) {
                            Integer diskon = Integer.parseInt(cekDiskon.getDiskon());
                            Integer nilai = hasilTestDto.getNilaiBiaya().getNilai().intValue() * diskon / 100;
                            model.addAttribute("nilai", hasilTestDto.getNilaiBiaya().getNilai().intValue());
                            model.addAttribute("diskon", diskon);
                            Integer nilaiDiskon = hasilTestDto.getNilaiBiaya().getNilai().intValue() - nilai;
                            model.addAttribute("nilaiDiskon",nilaiDiskon);
                            //CICILAN
                            //1x
                            model.addAttribute("cicil1x1", nilaiDiskon);
                            //2x
                            model.addAttribute("cicil2x1", nilaiDiskon * 50 / 100);
                            model.addAttribute("cicil2x2", nilaiDiskon * 50 / 100);
                            //3x
                            model.addAttribute("cicil3x1", nilaiDiskon * 40 / 100);
                            model.addAttribute("cicil3x2", nilaiDiskon * 30 / 100);
                            model.addAttribute("cicil3x3", nilaiDiskon * 30 / 100);
                            //4x
                            model.addAttribute("cicil4x1", nilaiDiskon * 40 / 100);
                            model.addAttribute("cicil4x2", nilaiDiskon * 20 / 100);
                            model.addAttribute("cicil4x3", nilaiDiskon * 20 / 100);
                            model.addAttribute("cicil4x4", nilaiDiskon * 20 / 100);

                            Integer totalAkhir = nilaiDiskon + hasilTestDto.getNilaiUkt().intValue()
                                    + hasilTestDto.getNilaiAsrama().intValue() + hasilTestDto.getNilaiWisuda().intValue();

                            model.addAttribute("totalAkhir", totalAkhir);

                            Integer perbulan = totalAkhir / 48;
                            model.addAttribute("perbulan", perbulan);
                        }
                    }else{
                        Integer nilai = hasilTestDto.getNilaiBiaya().getNilai().intValue();
                        model.addAttribute("nilai", hasilTestDto.getNilaiBiaya().getNilai().intValue());
                        model.addAttribute("diskon", "0");
                        model.addAttribute("nilaiDiskon", nilai);
                        //CICILAN
                        //1x
                        model.addAttribute("cicil1x1", nilai);
                        //2x
                        model.addAttribute("cicil2x1", nilai * 50 / 100);
                        model.addAttribute("cicil2x2", nilai * 50 / 100);
                        //3x
                        model.addAttribute("cicil3x1", nilai * 40 / 100);
                        model.addAttribute("cicil3x2", nilai * 30 / 100);
                        model.addAttribute("cicil3x3", nilai * 30 / 100);
                        //4x
                        model.addAttribute("cicil4x1", nilai * 40 / 100);
                        model.addAttribute("cicil4x2", nilai * 20 / 100);
                        model.addAttribute("cicil4x3", nilai * 20 / 100);
                        model.addAttribute("cicil4x4", nilai * 20 / 100);

                        Integer totalAkhir = nilai + hasilTestDto.getNilaiUkt().intValue()
                                + hasilTestDto.getNilaiAsrama().intValue() + hasilTestDto.getNilaiWisuda().intValue();

                        model.addAttribute("totalAkhir", totalAkhir);

                        Integer perbulan = totalAkhir / 48;
                        model.addAttribute("perbulan", perbulan);
                    }
                }else if (hasilTest.getJadwalTest().getPendaftar().getLeads().getJenjang().equals(Jenjang.S2)) {
                    if (hasilTestDto.getJadwalTest().getPendaftar().getLeads().getJenjang() == Jenjang.S2) {
                        if (hasilTestDto.getJadwalTest().getPendaftar().getProgramStudi().getId().equals("008") &&
                                hasilTestDto.getJadwalTest().getPendaftar().getKonsentrasi().equals("Reguler")) {
                            System.out.println("Konsentrasi Reguler : " + hasilTestDto.getJadwalTest().getPendaftar().getKonsentrasi());
                            NilaiBiaya nbMes = new NilaiBiaya();
                            nbMes.setNilai(new BigDecimal(6000000));
                            Integer nilai = nbMes.getNilai().intValue();
                            model.addAttribute("S2", nilai);
                            //CICILAN
                            //1x
                            model.addAttribute("cicil1x1", nilai);
                            //2x
                            model.addAttribute("cicil2x1", nilai * 50 / 100);
                            model.addAttribute("cicil2x2", nilai * 50 / 100);

                        } else if (hasilTestDto.getJadwalTest().getPendaftar().getKonsentrasi().equals("Eksekutif") &&
                                hasilTestDto.getJadwalTest().getPendaftar().getProgramStudi().getId().equals("008")) {
                            System.out.println("Konsentrasi Eksekutif : " + hasilTestDto.getJadwalTest().getPendaftar().getKonsentrasi());
                            NilaiBiaya nbMes = new NilaiBiaya();
                            nbMes.setNilai(new BigDecimal(7000000));
                            Integer nilai = nbMes.getNilai().intValue();
                            model.addAttribute("S2", nilai);
                            //CICILAN
                            //1x
                            model.addAttribute("cicil1x1", nilai);
                            //2x
                            model.addAttribute("cicil2x1", nilai * 50 / 100);
                            model.addAttribute("cicil2x2", nilai * 50 / 100);
                        } else {
                            Integer nilai = hasilTestDto.getNilaiBiaya().getNilai().intValue();
                            model.addAttribute("S2", nilai);
                            //CICILAN
                            //1x
                            model.addAttribute("cicil1x1", nilai);
                            //2x
                            model.addAttribute("cicil2x1", nilai * 50 / 100);
                            model.addAttribute("cicil2x2", nilai * 50 / 100);
                        }

                    }
                }
            }
        }else if (tagihan.getTotalPages() == 1){
            Page<Tagihan> tagihan1 = tagihanDao.findByPendaftarIdAndJenisBiayaId(pendaftar.getId(), jenisBiaya.getId(),page);
            model.addAttribute("tagihanLunas", tagihan1);

            for (Tagihan tg : tagihan1) {
                model.addAttribute("nama", tg.getPendaftar().getLeads().getNama());
                model.addAttribute("noReg", tg.getPendaftar().getNomorRegistrasi());
                model.addAttribute("prodi", tg.getPendaftar().getProgramStudi().getNama());
                model.addAttribute("konsen", tg.getPendaftar().getKonsentrasi());
                model.addAttribute("totalTagihan", tg.getTotalTagihan());

                BigDecimal sCicilan = cicilanDao.itungNominalCicilandanStatus(tg.getPendaftar(), Boolean.FALSE);
                BigDecimal tBelumLunas = tagihanDao.totalBayar(Boolean.FALSE,tg.getPendaftar());
                if (sCicilan != null && tBelumLunas != null) {
                    BigDecimal tSisa = new BigDecimal(sCicilan.intValue() + tBelumLunas.intValue());
                    model.addAttribute("sisaTagihan", tSisa);
                } else if (sCicilan != null && tBelumLunas == null) {
                    model.addAttribute("sisaTagihan", sCicilan);
                }else  if (sCicilan == null && tBelumLunas !=  null){
                    model.addAttribute("sisaTagihan", tBelumLunas);
                }

                BigDecimal tBayar = tagihanDao.totalBayar(Boolean.TRUE,tg.getPendaftar());
                model.addAttribute("totalBayar", tBayar);



//CEK HASIL TEST
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                model.addAttribute("keterangan", hasilTest.getKeterangan());

//Tagihan Belum Lunas
                if (tg.getLunas()) {
                    model.addAttribute("tgBlmLunas", tg.getNomorTagihan());
                }

                JenisBiaya jb = new JenisBiaya();
                jb.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);
                Tagihan tgblm = tagihanDao.findByPendaftarAndJenisBiayaAndLunas(tg.getPendaftar(), jb, false);
                model.addAttribute("virtualAccountBlm", virtualAccountDao.findByTagihan(tgblm, page));


                Pembayaran pembayaran = pembayaranDao.findByTagihan(tg);
                model.addAttribute("pembayaran", pembayaran);

                model.addAttribute("virtualAccount", virtualAccountDao.findByTagihan(tg, page));

                Page<VirtualAccount> virtualAccounts =  virtualAccountDao.findByTagihan(tg, page);
                for (VirtualAccount va : virtualAccounts) {
                    if (tg.getLunas() == true && pembayaran.getBank() == va.getBank()) {
                        model.addAttribute("vaBank", va.getBank());
                        model.addAttribute("vaNama", va.getBank().getNamaBank());
                        model.addAttribute("vaNo", va.getNomorVa());
                    } else {
                        logger.debug("Tagihan Belum Lunas");
                    }

                }

                model.addAttribute("pem",pembayaranDao.findByTagihanPendaftar(pendaftar, page));

//CICILAN
                model.addAttribute("cicilan", cicilanDao.findByPendaftarAndStatusOrderByUrutanCicilanAsc(tg.getPendaftar(),Boolean.FALSE, page));
                Integer jc = Integer.parseInt(tg.getCicilan());
                model.addAttribute("jumlahCicilan", jc);

                model.addAttribute("nilaiCicilan", tg.getNilai());

            }

        }
    }

    @PostMapping("/daftarUlang/formPendaftar")
    public String  prosesFormDuPendaftar(@RequestParam(value = "skema", required = true) String skema ,@Valid CicilanDto cicilanDto,
                                         Authentication currentUser) {
        logger.debug("Authentication class : {}", currentUser.getClass().getName());
        if (currentUser == null) {
            logger.warn("Current user is null");
        }
        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if (u == null) {
            logger.warn("Username {} not found in database ", username);
        }
        Leads leads = leadsDao.findByUser(u);
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);

        JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pendaftar);
        HasilTest hasilTest = hasilTestDao.findByJadwalTest(jadwalTest);
        hasilTest.setKeterangan(cicilanDto.getKeteranganDis());
        logger.info("Keterangan : {}",  cicilanDto.getKeteranganDis());
        hasilTestDao.save(hasilTest);

        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);

        if (skema.equals("skema1")) {
            if (cicilanDto.getCicilan().equals("1")) {
                logger.info("Jumlah cicilan {}", cicilanDto.getCicilan());
                //1X BAYAR
                Tagihan tgDu = tagihanDao.findByPendaftarAndJenisBiaya(pendaftar, jenisBiaya);
                if (tgDu == null) {
                    tagihanService.createTagihanDaftarUlang(pendaftar, cicilanDto.getC1x1(), cicilanDto.getCicilan());
                } else {
                    logger.info("Tagihan telah tersedia dengan nomor {}", tgDu.getNomorTagihan());
                }
            } else if (cicilanDto.getCicilan().equals("2")) {
                //2X BAYAR
                logger.info("Jumlah cicilann {}", cicilanDto.getCicilan());

                Tagihan tgDu = tagihanDao.findByPendaftarAndJenisBiaya(pendaftar, jenisBiaya);
                if (tgDu == null) {
                    insertCicilan(cicilanDto, pendaftar, u);

                    tagihanService.createTagihanDaftarUlang(pendaftar, cicilanDto.getC2x1(), cicilanDto.getCicilan());
                } else {
                    logger.info("Tagihan telah tersedia dengan nomor {}", tgDu.getNomorTagihan());
                }

            } else if (cicilanDto.getCicilan().equals("3")) {
                logger.info("Jumlah cicilannn {}", cicilanDto.getCicilan());
                //3X BAYAR

                Tagihan tgDu = tagihanDao.findByPendaftarAndJenisBiaya(pendaftar, jenisBiaya);
                if (tgDu == null) {
                    insertCicilan(cicilanDto, pendaftar, u);

                    tagihanService.createTagihanDaftarUlang(pendaftar, cicilanDto.getC3x1(), cicilanDto.getCicilan());
                } else {
                    logger.info("Tagihan telah tersedia dengan nomor {}", tgDu.getNomorTagihan());
                }

            } else if (cicilanDto.getCicilan().equals("4")) {
                logger.info("Jumlah cicilannnn {}", cicilanDto.getCicilan());

                Tagihan tgDu = tagihanDao.findByPendaftarAndJenisBiaya(pendaftar, jenisBiaya);
                if (tgDu == null) {
                    insertCicilan(cicilanDto, pendaftar, u);

                    tagihanService.createTagihanDaftarUlang(pendaftar, cicilanDto.getC4x1(), cicilanDto.getCicilan());
                } else {
                    logger.info("Tagihan telah tersedia dengan nomor {}", tgDu.getNomorTagihan());
                }
            }
        } else if (skema.equals("skema2")) {
            logger.info("Jumlah cicilannnn {}", "48 Bulan");

            Tagihan tgDu = tagihanDao.findByPendaftarAndJenisBiaya(pendaftar, jenisBiaya);
            if (tgDu == null) {
                insert48Cicilan(cicilanDto, pendaftar, u);

                tagihanService.createTagihanDaftarUlang(pendaftar, cicilanDto.getPerbulan(), "48");
            } else {
                logger.info("Tagihan telah tersedia dengan nomor {}", tgDu.getNomorTagihan());
            }
        }

//        if (pendaftar.getLeads().getJenjang().equals(Jenjang.S1)) {
//            return "redirect:/detailPendaftar/form";
//        } else if (pendaftar.getLeads().getJenjang().equals(Jenjang.S2)) {
//            return "redirect:/uploadBerkas/form";
//        }
        return "redirect:/daftarUlang/formPendaftar";
    }

    public void insert48Cicilan(CicilanDto cicilanDto, Pendaftar pendaftar, User u){
        Integer selesai = 48;
        for (Integer mulai = 2; mulai<= selesai; mulai++) {
            if (pendaftar != null) {
                Cicilan cicilan = new Cicilan();
                cicilan.setPendaftar(pendaftar);
                cicilan.setNominal(cicilanDto.getPerbulan());
                cicilan.setKeterangan("Cicilan Ke " + mulai + " dari 48 cicilan");
                System.out.println("Cicilan Ke " + mulai + " dari 48 cicilan");
                cicilan.setTanggalInsert(LocalDate.now());
                cicilan.setUserInster(u);
                cicilan.setUrutanCicilan(mulai.toString());
                cicilan.setStatus(Boolean.FALSE);
                cicilanDao.save(cicilan);
            } else {
                logger.info("Tagihannya daftar ulang {} tidak ditemukan", pendaftar.getNomorRegistrasi());
            }
        }
    }

    public void insertCicilan(CicilanDto cicilanDto, Pendaftar pendaftar, User u){
     if (cicilanDto.getCicilan().equals("2")) {
            //2X BAYAR
            logger.info("Jumlah cicilann {}", cicilanDto.getCicilan());
            LocalDate cc = LocalDate.now();
            if (pendaftar != null) {
                Cicilan cicilan = new Cicilan();
                cicilan.setPendaftar(pendaftar);
                cicilan.setNominal(cicilanDto.getC2x2());
                cicilan.setKeterangan("Cicilan Ke 2 dari 2 cicilan");
                cicilan.setTanggalInsert(LocalDate.now());
                cicilan.setUserInster(u);
                cicilan.setUrutanCicilan("2");
                cicilan.setStatus(Boolean.FALSE);
                cicilan.setTanggalKirim(cc.plusMonths(2));
                cicilanDao.save(cicilan);
                logger.info("Nominal Tagihan Daftar Ulang cicil 2x2 {}", cicilanDto.getC2x2());
            } else {
                logger.info("Tagihannya daftar ulang {} tidak ditemukan", pendaftar.getNomorRegistrasi());
            }
        } else if (cicilanDto.getCicilan().equals("3")) {
            logger.info("Jumlah cicilannn {}", cicilanDto.getCicilan());

            LocalDate cc = LocalDate.now();

            logger.info("Cek DU : {}", pendaftar);
            if (pendaftar != null) {
                Cicilan cicilan = new Cicilan();
                cicilan.setPendaftar(pendaftar);
                cicilan.setNominal(cicilanDto.getC3x2());
                cicilan.setKeterangan("Cicilan Ke 2 dari 3 cicilan");
                cicilan.setTanggalInsert(LocalDate.now());
                cicilan.setUserInster(u);
                cicilan.setUrutanCicilan("2");
                cicilan.setStatus(Boolean.FALSE);
                cicilan.setTanggalKirim(cc.plusMonths(2));
                cicilanDao.save(cicilan);
                logger.info("Nominal Tagihan Daftar Ulang cicil 3x2 {}", cicilanDto.getC3x2());

                Cicilan cicilan2 = new Cicilan();
                cicilan2.setPendaftar(pendaftar);
                cicilan2.setNominal(cicilanDto.getC3x3());
                cicilan2.setKeterangan("Cicilan Ke 3 dari 3 cicilan");
                cicilan2.setTanggalInsert(LocalDate.now());
                cicilan2.setUserInster(u);
                cicilan2.setUrutanCicilan("3");
                cicilan2.setStatus(Boolean.FALSE);
                cicilan2.setTanggalKirim(cc.plusMonths(4));
                cicilanDao.save(cicilan2);
                logger.info("Nominal Tagihan Daftar Ulang cicil 3x3 {}", cicilanDto.getC3x3());
            } else {
                logger.info("Tagihannya daftar ulang {} tidak ditemukan", pendaftar.getNomorRegistrasi());
            }

        } else if (cicilanDto.getCicilan().equals("4")) {
            logger.info("Jumlah cicilannnn {}", cicilanDto.getCicilan());

            LocalDate cc = LocalDate.now();
            logger.info("Cek DU : {}", pendaftar);
            if (pendaftar != null) {
                Cicilan cicilan = new Cicilan();
                cicilan.setPendaftar(pendaftar);
                cicilan.setNominal(cicilanDto.getC4x2());
                cicilan.setKeterangan("Cicilan Ke 2 dari 4 cicilan");
                cicilan.setTanggalInsert(LocalDate.now());
                cicilan.setUserInster(u);
                cicilan.setUrutanCicilan("2");
                cicilan.setStatus(Boolean.FALSE);
                cicilan.setTanggalKirim(cc.plusMonths(2));
                cicilanDao.save(cicilan);
                logger.info("Nominal Tagihan Daftar Ulang cicil 4x2 {}", cicilanDto.getC4x2());

                Cicilan cicilan2 = new Cicilan();
                cicilan2.setPendaftar(pendaftar);
                cicilan2.setNominal(cicilanDto.getC4x3());
                cicilan2.setKeterangan("Cicilan Ke 3 dari 4 cicilan");
                cicilan2.setTanggalInsert(LocalDate.now());
                cicilan2.setUserInster(u);
                cicilan2.setUrutanCicilan("3");
                cicilan2.setStatus(Boolean.FALSE);
                cicilan2.setTanggalKirim(cc.plusMonths(4));
                cicilanDao.save(cicilan2);
                logger.info("Nominal Tagihan Daftar Ulang cicil 4x3 {}", cicilanDto.getC4x3());

                Cicilan cicilan3 = new Cicilan();
                cicilan3.setPendaftar(pendaftar);
                cicilan3.setNominal(cicilanDto.getC4x4());
                cicilan3.setKeterangan("Cicilan Ke 4 dari 4 cicilan");
                cicilan3.setTanggalInsert(LocalDate.now());
                cicilan3.setUserInster(u);
                cicilan3.setUrutanCicilan("4");
                cicilan3.setStatus(Boolean.FALSE);
                cicilan3.setTanggalKirim(cc.plusMonths(6));
                cicilanDao.save(cicilan3);
                logger.info("Nominal Tagihan Daftar Ulang cicil 4x4 {}", cicilanDto.getC4x4());
            } else {
                logger.info("Tagihannya daftar ulang {} tidak ditemukan", pendaftar.getNomorRegistrasi());
            }

        }
    }

    @Scheduled(cron = "${scheduled.delete.tagihan.expired}", zone = "Asia/Jakarta")
    public String tagihanExpired(){

        JenisBiaya jb = new JenisBiaya();
        jb.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);

        Iterable<Pendaftar> pe = pendaftarDao.findByTahunAjaranAktif(Status.AKTIF);

        for (Pendaftar pendaftar : pe) {

            Iterable<Tagihan> cekTagihan = tagihanDao.cekTagihan(pendaftar.getId(), jb.getId());

            for (Tagihan tagihan : cekTagihan) {
                //Hapus Cicilan
                Iterable<Cicilan> cicilan = cicilanDao.findAllByPendaftar(tagihan.getPendaftar());
                for (Cicilan cc : cicilan) {
                    cicilanDao.delete(cc);
                    logger.info("Hapus cicilan : " + cc.getPendaftar().getLeads().getNama());
                }

                //Delete di aplikasi SPMB
                Iterable<VirtualAccount> virtualAccounts = virtualAccountDao.findByTagihan(tagihan);
                for (VirtualAccount va : virtualAccounts) {
                    virtualAccountDao.delete(va);
                    logger.info("Hapus Nomor Va : " + va.getTagihan().getPendaftar().getLeads().getNama());
                }
                //Delete tagihan Pmb
                tagihanDao.delete(tagihan);

                //Delete di aplikasi tagihan
                tagihanService.hapusTagihan(tagihan);
                logger.info("Nomor tagihan expired  {}, dengan nama {} ", tagihan.getNomorTagihan(), tagihan.getPendaftar().getLeads().getNama());
            }
        }
        return "/";

    }


    @Scheduled(cron = "${scheduled.kirim.tagihan.cicilan}", zone = "Asia/Jakarta")
    public void kirimTagihanCicilan(){
        Iterable<Pendaftar> pe = pendaftarDao.findByTahunAjaranAktif(Status.AKTIF);

        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);

        Iterable<Tagihan> tagihan = tagihanDao.cekTagianCicilan(jenisBiaya);
        for (Tagihan tg : tagihan) {
            Iterable<Cicilan> cc = cicilanDao.cekCicilan(tg.getPendaftar());
            logger.info("Tagihannya : {}", tg.getPendaftar().getNomorRegistrasi());
            for (Cicilan cicilan : cc) {
                logger.info("Cicilan yang dikirim hari ini {}, nominal {}", cicilan.getPendaftar().getNomorRegistrasi(), cicilan.getNominal());

                //update status cicilan
                cicilan.setStatus(true);
                cicilanDao.save(cicilan);
                logger.info("Update status cicilan : {}", cicilan.getPendaftar().getNomorRegistrasi());

                //Delete diaplikasi Tagihan
                tagihanService.hapusTagihan(tg);

                tagihanService.createTagihanDaftarUlang(tg.getPendaftar(), cicilan.getNominal(), tg.getCicilan());

            }
        }

    }

}
