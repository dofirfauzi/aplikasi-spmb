package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Pekerjaan;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PekerjaanDao extends PagingAndSortingRepository<Pekerjaan, String> {

}
