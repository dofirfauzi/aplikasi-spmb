package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Controller
public class JadwalTestController {
    private static final Logger logger = LoggerFactory.getLogger(JadwalTestController.class);

    @Autowired private UserDao userDao;
    @Autowired private LeadsDao leadsDao;
    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private JadwalTestDao jadwalTestDao;
    @Autowired private PembayaranController pembayaranController;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private HasilTestDao hasilTestDao;

    @GetMapping("/jadwalTest/form")
    public String formJadwalTest(Model model,  Authentication currentUser) {

        logger.debug("Authentication class : {}", currentUser.getClass().getName());

        if (currentUser == null) {
            logger.warn("Current user is null");
        }

        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if (u == null) {
            logger.warn("Username {} not found in database ", username);
        }

        Leads leads = leadsDao.findByUser(u);
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        if (pendaftar != null) {
            model.addAttribute("pendaftar", pendaftar);
        }
        JadwalTest jadwalTest = new JadwalTest();

        JadwalTest jt = jadwalTestDao.findByPendaftar(pendaftar);
        if (jt == null){
            model.addAttribute("jadwalTest", jadwalTest);

        }else {
            model.addAttribute("jadwalTest", jt);
        }

//Cek Pembayaran
        Pembayaran pembayaran = pembayaranController.cekPembayaran(pendaftar);
        model.addAttribute("pembayaran", pembayaran);

        HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
        model.addAttribute("hasilTest", hasilTest);

        return "jadwalTest/form";
    }

    @PostMapping("/jadwalTest/form")
    public String prosesJadwalTest(@ModelAttribute @Valid JadwalTest jadwalTest, Authentication currentUser, Model model){
        logger.debug("Authentication class : {}", currentUser.getClass().getName());
        if (currentUser == null) {
            logger.warn("Current user is null");
        }

        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if (u == null) {
            logger.warn("Username {} not fount in database", username);
        }
        Leads leads = leadsDao.findByUser(u);
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);

        jadwalTest.setPendaftar(pendaftar);
        jadwalTestDao.save(jadwalTest);
        logger.info("Jadwal test dengan no pendaftar {} berhasil di simpan", pendaftar.getNomorRegistrasi());

        return "redirect:/kartuUjian/form";

    }

    @GetMapping("/jadwalTest/list")
    public void listJadwalTest(@RequestParam(required = false)String nama,@RequestParam(required = false)String smart, Model m,@Qualifier("tjp") Pageable tjpPage, @Qualifier("smartTest") Pageable stPage){
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer bulan = localDate.getMonthValue();

        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarJadwal", jadwalTestDao.findJadwalTest(nama,tahunAjaran,JenisTest.SMART_TEST,tjpPage));
        } else {
            m.addAttribute("daftarJadwal", jadwalTestDao.findJadwalTestAll(tahunAjaran,JenisTest.SMART_TEST,tjpPage));
        }

        if(StringUtils.hasText(smart)) {
            m.addAttribute("smart", smart);
            m.addAttribute("daftarJadwalSt", jadwalTestDao.findJadwalTestSt(smart,bulan,tahunAjaran,JenisTest.SMART_TEST,stPage));
        } else {
            m.addAttribute("daftarJadwalSt", jadwalTestDao.findJadwalTestSmart(bulan,tahunAjaran,JenisTest.SMART_TEST,stPage));
        }

        Iterable<JadwalTest> jadwalTest = jadwalTestDao.findAll();
        for (JadwalTest jt : jadwalTest) {
            HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
            m.addAttribute("hasilTest", hasilTest);
        }




    }
}
