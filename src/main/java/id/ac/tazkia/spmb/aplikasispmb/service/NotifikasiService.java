package id.ac.tazkia.spmb.aplikasispmb.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.spmb.aplikasispmb.dao.LeadsDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.PendaftarDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.DataResetPassword;
import id.ac.tazkia.spmb.aplikasispmb.dto.NotifikasiResetPassword;
import id.ac.tazkia.spmb.aplikasispmb.entity.Leads;
import id.ac.tazkia.spmb.aplikasispmb.entity.Pendaftar;
import id.ac.tazkia.spmb.aplikasispmb.entity.ResetPassword;
import id.ac.tazkia.spmb.aplikasispmb.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class NotifikasiService {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotifikasiService.class);

    @Value("${kafka.topic.notifikasi}") private String topicNotifikasi;
    @Value("${notifikasi.registrasi.konfigurasi.it-reset-password}") private String getKonfigurasiNotifikasiReset;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired private ObjectMapper objectMapper;
    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private LeadsDao leadsDao;

    @Async
    public void resetPassword(ResetPassword p) {
        User user = p.getUser();
        Leads leads = leadsDao.findByUser(user);
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        NotifikasiResetPassword notif = NotifikasiResetPassword.builder()
                .konfigurasi(getKonfigurasiNotifikasiReset)
                .email(pendaftar.getLeads().getEmail())
                .data(DataResetPassword.builder()
                        .code(p.getCode())
                        .nama(p.getUser().getUsername())
                        .build())
                .build();

        try {
            kafkaTemplate.send(topicNotifikasi, objectMapper.writeValueAsString(notif));
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
        }
    }

}
