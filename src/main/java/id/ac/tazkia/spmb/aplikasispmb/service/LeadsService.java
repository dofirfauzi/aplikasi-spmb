package id.ac.tazkia.spmb.aplikasispmb.service;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.LeadsDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service @Transactional
public class LeadsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(LeadsService.class);


    @Autowired private LeadsDao leadsDao;
    @Autowired private UserDao userDao;
    @Autowired private RoleDao roleDao;
    @Autowired private UserPasswordDao userPasswordDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private PasswordEncoder passwordEncoder;



    public String registrasiLeads(LeadsDto leadsDto){
        Leads leads = new Leads();

//        Leads cekLeads = leadsDao.cariUsername("%"+leadsDto.getUsername().toLowerCase()+"%");
        User cekLeads = userDao.findByUsername(leadsDto.getEmail());

        if (cekLeads == null) {
            //CreateUser&Password
            User user = new User();
            Role role = roleDao.findById(AppConstants.ROLE_PENDAFTAR).get();

            user.setUsername(leadsDto.getEmail());
            user.setActive(true);
            user.setRole(role);
            userDao.save(user);

            UserPassword password = new UserPassword();
            password.setUser(user);
            password.setPassword(passwordEncoder.encode(leadsDto.getPassword()));
            userPasswordDao.save(password);
            //

            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            leads.setTahunAjaran(ta);
            leads.setUser(user);

            BeanUtils.copyProperties(leadsDto, leads);
            leadsDao.save(leads);

            LOGGER.info("Leads a.n {} Berhasiil di simpan", leads.getNama());
        } else {
            LOGGER.info("Username a.n {} sudah ada", cekLeads.getUsername());

        }


        return "redirect:/selesai";

    }

}
