package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.ProgramStudi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProgramStudiDao extends PagingAndSortingRepository<ProgramStudi, String> {
    Page<ProgramStudi> findByNamaContainingIgnoreCaseOrderByNama(String nama, Pageable page);
    @Query("select p from ProgramStudi p where p.id >= :id and p.id <= :dua")
    List<ProgramStudi> cariProdi(@Param("id")String id,@Param("dua")String dua);

    @Query("select p from ProgramStudi p where p.id <= :id and p.id != :tidak and p.id != :tidak2")
    List<ProgramStudi> cariProdiS1(@Param("id")String id,@Param("tidak")String tidak,@Param("tidak2")String tidak2);

    @Query("select p from ProgramStudi p where p.id = :id")
    List<ProgramStudi> cariProdiD3(@Param("id")String id);
}
