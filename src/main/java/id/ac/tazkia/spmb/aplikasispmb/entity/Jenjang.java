package id.ac.tazkia.spmb.aplikasispmb.entity;

public enum Jenjang {
    S1,
    S2,
    D3
}
