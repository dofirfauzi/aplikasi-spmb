package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.GradeDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.SmartTestDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.UserDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.HasilTestDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.SmartTestDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.UploadError;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class SmartTestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TagihanController.class);

    @Autowired
    private UserDao userDao;
    @Autowired
    private SmartTestDao smartTestDao;

    @Value("classpath:sample/sample_smartTest.csv")
    private Resource contohFileSmartTest;

    @Autowired
    private GradeDao gradeDao;
    @Autowired
    private HasilTestController hasilTestController;


    @GetMapping("/contoh/smartTest")
    public void downloadContohFileTagihan(HttpServletResponse response) throws Exception {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=sample_smartTest.csv");
        FileCopyUtils.copy(contohFileSmartTest.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

    @GetMapping("/smartTest/form")
    public void formSmartTest (@RequestParam(required = false)String cari, Model m,
                               @PageableDefault(sort = "nama", direction = Sort.Direction.DESC) Pageable page){

        Iterable<SmartTest> smartTests = smartTestDao.findAll();
        List<SmartTestDto> smartTestDtos = new ArrayList<>();

        for (SmartTest smartTest : smartTests) {
           Grade hitungGrade = hasilTestController.hitungGrade(smartTest.getNilai());
           SmartTestDto smtDto = new SmartTestDto();
           smtDto.setNama(smartTest.getNama());
           smtDto.setAsalSekolah(smartTest.getAsalSekolah());
           smtDto.setTanggalTest(smartTest.getTanggalTest());
           smtDto.setGrade(hitungGrade);
           smartTestDtos.add(smtDto);

        }
        m.addAttribute("gS",smartTestDtos);

        if(StringUtils.hasText(cari)) {
            m.addAttribute("cari", cari);
            m.addAttribute("smartTest", smartTestDao.findByNamaContainingIgnoreCaseOrAsalSekolahContainingIgnoreCase(cari,cari, page));
        } else {
            m.addAttribute("smartTest", smartTestDao.findAll(page));
        }
    }

    @PostMapping("/smartTest/form")
    public String processFormUpload(@RequestParam String  asalSekolah, @DateTimeFormat(pattern = "yyyy-MM-dd") Date tanggalTest,
                                    @RequestParam(required = false) Boolean pakaiHeader,
                                    MultipartFile fileSmartTest, RedirectAttributes redirectAttrs, Authentication currentUser){

        LOGGER.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            LOGGER.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        LOGGER.debug("User ID : {}", u.getId());
        if(u == null){
            LOGGER.warn("Username {} not found in database ", username);
        }

        LOGGER.debug("Asal Sekolah : {}",asalSekolah);
        LOGGER.debug("Tanggal Test : {}",tanggalTest);
        LOGGER.debug("Pakai Header : {}",pakaiHeader);
        LOGGER.debug("Nama File : {}",fileSmartTest.getName());
        LOGGER.debug("Ukuran File : {}",fileSmartTest.getSize());

        List<UploadError> errors = new ArrayList<>();
        Integer baris = 0;

        if(asalSekolah == null){
            errors.add(new UploadError(baris, "Asal sekolah harus diisi", ""));
            redirectAttrs
                    .addFlashAttribute("jumlahBaris", 0)
                    .addFlashAttribute("jumlahSukses", 0)
                    .addFlashAttribute("jumlahError", errors.size())
                    .addFlashAttribute("errors", errors);
            return "redirect:hasil";
        }
        if(tanggalTest == null){
            errors.add(new UploadError(baris, "Tanggal test harus diisi", ""));
            redirectAttrs
                    .addFlashAttribute("jumlahBaris", 0)
                    .addFlashAttribute("jumlahSukses", 0)
                    .addFlashAttribute("jumlahError", errors.size())
                    .addFlashAttribute("errors", errors);
            return "redirect:hasil";
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileSmartTest.getInputStream()));
            String content;

            if((pakaiHeader != null && pakaiHeader)){
                content = reader.readLine();
            }

            while ((content = reader.readLine()) != null) {
                baris++;
                String[] data = content.split(",");
                if (data.length != 2) {
                    errors.add(new UploadError(baris, "Format data salah", content));
                    continue;
                }

                // Save table smartTest
                SmartTest p = new SmartTest();
                p.setNama(data[0]);
                p.setAsalSekolah(asalSekolah);
                p.setNilai(new BigDecimal(data[1]));
                p.setTanggalTest(tanggalTest);
                smartTestDao.save(p);

            }
        } catch (Exception err){
            LOGGER.warn(err.getMessage(), err);
            errors.add(new UploadError(0, "Format file salah", ""));
        }

        redirectAttrs
                .addFlashAttribute("jumlahBaris", baris)
                .addFlashAttribute("jumlahSukses", baris - errors.size())
                .addFlashAttribute("jumlahError", errors.size())
                .addFlashAttribute("errors", errors);

        return "redirect:hasil";
    }

    @GetMapping("/smartTest/hasil")
    public void hasilFormUpload() {
    }

    @GetMapping("/smartTest/listIndex")
    public void listSmartTest(@RequestParam(required = false) String nama, @RequestParam(required = false) String sekolah
                              ,Model m, @PageableDefault(size = 15, sort = "nama", direction = Sort.Direction.ASC)Pageable page){

        if (nama != null && sekolah != null) {
            m.addAttribute("nama", nama);
            m.addAttribute("sekolah", sekolah);
            m.addAttribute("listSmart",smartTestDao.findByNamaContainingIgnoreCaseAndAsalSekolahContainingIgnoreCase(nama,sekolah,page));
        } else if (nama != null && sekolah == null) {
            m.addAttribute("nama", nama);
            m.addAttribute("listSmart",smartTestDao.findByNamaSt(nama,page));
        } else if (nama == null && sekolah != null) {
            m.addAttribute("sekolah", sekolah);
            m.addAttribute("listSmart",smartTestDao.findByAsalSekolahContainingIgnoreCase(sekolah, page));
        } else {
            m.addAttribute("listSmart",smartTestDao.findAll(page));
        }

    }
}
