CREATE TABLE smart_test (
id            VARCHAR(36),
nama          VARCHAR(100) NOT NULL,
asal_sekolah  VARCHAR(255) NOT NULL,
nilai         NUMERIC(5,2) not NULL default 0,
tanggal_test  DATE NOT NULL,
PRIMARY KEY (id));